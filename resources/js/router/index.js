import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import TodoList from '../components/TodoList';
import AddTask from '../components/AddTask';

const routes = [
    { path: '/tasks', component: TodoList },
    { path: '/add-task', component: AddTask },
];

const index = new VueRouter({
    routes,
    mode: 'history',
});

export default index;
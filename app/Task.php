<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    const TODO = 0;
    const IN_PROGRESS = 1;
    const DONE = 2;

    const STATUSES = [self::TODO, self::IN_PROGRESS, self::DONE];

    /**
     * @var array
     */
    protected $fillable = ['content', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
